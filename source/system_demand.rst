===================
System Demand
===================
A round of upgrades was finished at benchmark meteorological stations in 2015 with the goal of promoting
data accessibility, allowing connectivity to data loggers, and improving measurement methodology,  while 
ensuring data security. Network connectivity was a large part of the addition. This allowed multiple loggers
to connect to a network, doing away with long cable
connections within stations. It also allows for greater access and control of
logger function. Another large addition was aspirated radiation shields which increase measurement accuracy. 
Below, the power demands of this new network are assessed in two parts: 1) the current requirements of the 
system, 2) the power available to the system. Systemic liabilities to the power 
supply are discussed, along with some possible solutions. 

Electrical Draw
----------------
Estimation Methods
^^^^^^^^^^^^^^^^^^^
Daily Current vs. Peak Current
""""""""""""""""""""""""""""""""
The system draw or current is a measure of the flow of electrical charge (Ampere). Inherently, this is a rate 
that 
fluctuates as more or less electricity is required. For example, as components turn on and off. To calculate
the amount of charge used, the rate must be multiplied by the length of time that the component is turned
on. 

Below, the current is discussed as peak current or peak draw in Amps (A). This represents the highest rate 
at which the 
the system will draw a charge. This is an important factor in sizing wiring and other components, and can have a 
large effect on how long a battery can provide power.

Below, the daily draw is also discussed, in Amp hours (Ah). Since different components are turned on for 
different amounts of time, 
the peak current does not represent the amount charge required. The daily draw provides an aggregate estimate of total
draw which accounts for how long each component
is turned on, and what the current is for that component.

Campbell Model
""""""""""""""""
`Campbell Scientific`_ (CSI) provided an Excel macro [#]_ to estimate system demand and solar power source. Components and
sensors sold by CSI can be directly input, and unique components can be manually entered. This tool was used to 
produce general estimates of charge required by the met stations. To add custom components, the following 
specifications are required:

1. Active Current (A)- current when turned on
2. Quiescent Current (A)- current when turned off
3. Active Time/Scan (mSecs)

When these specifications were not available "ball park" estimates were used.
Below is a list of custom parameters input:

* MET stations are on 15 sec scan, which is not one of the provided options. 10 sec is used instead. This inflates estimates.
* Radios are always on. Active time set to 10,000- info from Adam Kennedy
    :Exception: Xtend-NEMA tests connection for 1 sec every 16 sec, and downloads for 1 min per hour
* Radio power is an average from independent tests provided by Adam Kennedy in Watts
	* :math:`I_{A} = \frac{P_{W}}{V} = \frac{7}{24}`
* CSI model CSAT3_ was substituted for the `WindObserverII Ultrasonix Anemometer`_
	* the WindObserverII- 40mA active, no quiescent listed
	* CSAT3- 67mA active and quiescent
* LT420 Float Gauge 
	* Active time set as 50 mSec active per scan. No value listed in manual
	* Active current set to 20mA. Factory calibration specified as 20mA
	* Quiescent current set as 10% of active current
* The NL115 was substituted for the NL116. Estimates include draw of both ethernet mode and card mode separately
* Current to activate the pump switch at standalone rain gauges was defined as 1.6mA by CSI (Bruce)
	* Active time was set as 210 mSec = :math:`\frac{15 cycles * \frac{2 min}{cycle}}{24 hr} = 0.021 * 10,000 mSec`
* When the station had more devices than the macro allows, components that drew < 1% of the daily current were removed

The CSI format was mimicked in a separate spreadsheet. This independent power budget [#]_
consistently
estimates less than the CSI macro by 16% for shelters and 24% for towers. However, by including a fuller
suite of sensors, the contribution of individual components relative to each other provides useful insight.

.. [#] CSI PowerBudget13_v1 :download:`Spreadsheet<./_references/Power Budget 13 V1.xlsm>` `, web how to <https://www.campbellsci.com/videos?video=58>`_
.. [#] Independent Power Budget  :download:`Summary Spreadsheet<./_references/Budget_Summary.xlsx>`

Draw Summary
^^^^^^^^^^^^^
Here is an overview of common system components using CSI estimates. Each station will be further discussed 
individually. Estimates are based on station configuration at the beginning of 2016. Aspirated fans have
independent power supplies and are listed separately.

==========	=================	===============
Station		System Daily (Ah)	System Peak (A)
==========	=================	===============
CENT Shltr	3.23				0.112
CENT Twr	12.06				0.419
UPLO Shltr	6.91				0.240
UPLO Twr	13.19				0.458
VARA 		10.38				0.361
VAN Twr		12.43				0.432
ASP Fan		14.63				0.508				
==========	=================	===============

The highest daily draws result from components that are on for long periods of time. These components account 
for disproportionate amounts of the system current demand. Below is a list of the components with the 
highest daily draws, taken from the independent spread sheet.

================    =================   ================
Component           Daily (Ah)          Peak (A)
================    =================   ================
ASP Fan				12.19				0.5
Radio- Tranzeo		7					0.292
Radio- Nano			3.98				0.166
Sonic Anemometer	0.96				0.04
Radio- XtendNEMA    0.73                0.11
NL115 Card			0.55				0.023
NL115 Ethernet		0.48				0.02
Xantrex C35			0.31				0.013
SR50A				0.26				0.25
SunSaver 10			0.192				0.008
================    =================   ================

.. Note::
	Radios (**except NEMA**) require a conversion to 24v. The current provided above is after the conversion to 24v.
	This underestimates
	the system draw because >10% of power is lost during conversion.

The highest draw is from components that run continuously. While the aspirated fan must run all the time, the radios
are only transmitting for a fraction of the time that they are powered. For example, extend NEMA takes a minute to
download an hours worth of data. Powering the device while idle is wasted energy.

Also, note that the Xantrex charge controller uses almost double the current of the SunSaver. It is important to
assess efficiency when buying components.


Power Supply
--------------
Each station has a different configuration of solar panels and batteries, ranging in size, age, and number. 
These will be discussed individually for each site, but a broad overview is presented here. Stand alone rain gauges
are not discussed. Their structure has been static for many years. They have the oldest solar panels, but seem to work,
and as a result, they are lower priority at this time.

Now, with estimates of the current required to run the system, it is important to look at the present power supply and
storage. Power supply is determined by solar panel rating, reduction in solar panel function with age, and available
sunlight. Storage is determined by the battery bank. During winter sunlight has shorter duration and intensity. Under
these conditions, solar panels produce small amounts of power, requiring more solar panels to create enough energy. This
must be balanced against the size of the battery bank for storing energy during periods when solar panels are
ineffective.

Solar Power Production
^^^^^^^^^^^^^^^^^^^^^^^
At a minimum, solar power must generate enough current to exceed daytime demand by enough to recharge batteries from
night time
draw. Below is the sum of solar panel wattage at each site and total daily power requirements (in Watts hours).

==========	=================	===============
Station		System Daily (Wh)	Solar Panel(W)
==========	=================	===============
CENT Shltr	38.2				50.2
CENT Twr	144.72				200
CENT ASP	**175.56**			**100**
UPLO Shltr	82.9				240
UPLO Twr	158.28				260
UPLO ASP	**175.56**			**100**
VARA 		124.56				131
VAN Twr		149.16				210
VAN ASP 	**175.56**			**52.6**
==========	=================	===============

Each solar panel specifies reference conditions needed to produce it's rated wattage. While some of the oldest panels
differ slightly,
most solar panels require 1000 W :math:`m^{-2}` to produce their rated power. For stations where the solar panels meet or
exceed estimated power requirements, only 1 hour of sunlight is required at this intensity. If the rating is less than
estimates of power needs, than more time is needed at the specified intensity.

It is clear from this quick overview that the current allocation of solar panels is not proportional to power needs.
The power requirements of aspirated fans are as much as 1.75 - 3.5 times more than the rating of their solar panels.
During much of the year, sites do not cumulatively receive 1 kWh of sunlight a day, indicating that the fans are not
fully powered. UPLO shelter, however, has solar panels rated at 3 times it's daily power needs.

.. note::
    Batteries are not 100% efficient. If 10 Wh are drawn from a battery overnight, it may take >10Wh to restore the
    charge to the battery the next day.

    Also

    Solar panels loose an average of 1% of capacity per year [#]_. The oldest solar panels may produce approximately
    20% less than rated.

.. [#] :download:`Jordan, D.C. and S.R. Kurtz.<./_references/SolarPanel_longevity.pdf>` 2012. Photovoltaic degradation rates- an analytical review. JA-5200-51664, NREL. Oakridge, TN.

Battery Bank
^^^^^^^^^^^^^
The minimum requirement for the battery bank is that it be large enough to supply current during nighttime hours.
During winter the batteries must supply more than 0.75 of the daily current requirement. During storm cycles, they may
need to supply charge for longer.

Stations nearly exclusively use :download:`USBattery 1800 XC2<./_references/USBattery_XC1800_data.pdf>`.

==========	=================	================    ================    ======================
Station		System Daily (Ah)	Battery Bank(Ah)    Days to 30% DOD     Day to Total Discharge
==========	=================	================    ================    ======================
CENT Shltr	3.23				416                 >14                 48.8
CENT Twr	12.06				624                 10                  36
CENT ASP	14.63   			416                 **6**               **17.4**
UPLO Shltr	6.91				416                 11                  36.9
UPLO Twr	13.19				624                 10                  32.6
UPLO ASP	14.63   			624                 9                  34.9
VARA 		10.38				208                 **4**               **12.4**
VAN Twr		12.43				1040                >14                 73
VAN ASP 	14.63   			416                 **6**               **17.4**
==========	=================	================    ================    ======================

Bold values highlight stations with extremely low storage. These stations are unlikely to survive a single bad storm
cycle without substantial solar panels.

Similarly to solar panels, batteries are not allocated proportionally to system draw. Aspirated fans have the highest
draw, but have undersized battery banks, while
several stations have oversized battery banks. Aspirated fans in particular don't have adequate solar panels to meet
daily power needs, and have undersized battery banks. It is possible that these components regularly exceed their power
supply, but they are unmonitored, so there is no way to confirm system draw, battery voltage, or whether they are
turned on.

.. Note::
    Battery Ah need to be adjusted for temperature.

.. _Campbell Scientific : https://www.campbellsci.com/contact
.. _CSAT3 : https://www.campbellsci.com/csat3
.. _WindObserverII Ultrasonix Anemometer : http://gillinstruments.com/products/anemometer/wind_ob2.htm