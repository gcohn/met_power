********************
System Improvements
********************

Systemic Problems
------------------
Many components of the MET stations are old, and undersized. However simply adding solar panels and batteries, will not
reduce the chance of system failure and would easily exceed the budget. The present station design is built around
production of data.
However, it ignores the operation of the system that
collects the data. This risks generating erroneous data and allowing the system to crash. It also means that power
supply is often not where it is needed. The stations need to
be designed to support the function of the system as a whole. It would be difficult to supply enough power to match
current demand, but it is possible to maintain current function with a lower power requirement.

Key tenants of design are:

1. Monitoring all devices
    a. Track system power generation and draw
    b. Make it easy to identify system malfunctions
    c. Make it easy to anticipate loss of power
2. Provide redundant operations
    a. Multiple power sources (e.g. multiple charge controllers)
    b. Multiple  ways to restrict components (e.g. turn radios off)
    c. Multiple data backups
3. Protecting equipment from degradation
    a. e.g. proper cooling for charge controllers
    b. e.g. controlled phase down of power usage when power generation is low
4. Intelligently allocate power
    a. Careful monitoring allows power to be intelligently distributed to components that need it most
    b. Monitoring can be used to trigger reallocation of power as conditions require

:Example:

    For example, 2015 a charge control went bad at UPLO. This overdischarged the batteries, ruining them. It also cut
    power to the
    logger, which stopped data collection and allowed the rain gauge to freeze. Better monitoring of power supply would
    have identified that there was a problem. A secondary power source would have kept the station running while the
    faulty controller was fixed. Redundant programatic and manual switches would have allowed high draw components such as
    radios to be
    disconnected, conserving power. A low voltage disconnect could have further protected the batteries. Finally, if the
    charge
    controller were stored in a dry, well ventilated area away from caustic battery gases, it may not have failed at all.
    This clearly demonstrates the need for system upgrades to focus not just on power, but on function.

**Nearly 80% reduction in power needs** can be achieved with changes to radio and power configurations. This eases the
power requirements and allows focus to be directed at system liabilities such as:

* Monitoring power usage
* Providing manual and programmatic controls over power use
* Improving charge controllers
    * Add LVD's
    * Protect charge controllers from moisture and overheating
* Improving protection of degraded wiring

Proposed Improvements
----------------------
Each station is uniquely configured and has special problems which should be addressed. Here I discuss large scale
functional changes that apply to all of the met stations, and attempt to estimate the power savings and the monetary
cost.

Monitor System Current
^^^^^^^^^^^^^^^^^^^^^^^
All loggers will have a shunt installed to track logger current. This allows close monitoring of system operations,
battery depth of discharge, and number of battery cycles. Number of battery cycles is the strongest indicator of battery
lifespan and number of Ah discharged is the only way to confidently measure depth of discharge.
This is key in identifying when a station may be approaching loss of power. This information creates the opportunity for
the logger
to take steps to protect essential components.

Shunts can also be installed on
the charge controllers to track solar panel output, however some loggers do not have enough open ports to support this.

Data will be logged to an independent *POWER* table. To assess power levels, solar panel function, and battery
health will require both onboard and server-side calculations.

.. todo::
    This requires changes to the program logic, and the installation and purchase of a shunt.

:Cost:
    Quantity: minimum of 1 shunt is needed per logger.

    Price: a good quality sealed shunt can cost from $7 to $35

    Total: **$40 - $200**

Combine Power Supply
^^^^^^^^^^^^^^^^^^^^^
Currently the aspirated fans are run on an isolated power supply. This has the benefit of isolating the logger from any
power issues the fan has. However, this also prevents the logger from knowing whether or not the fan is turned on.
Combining power supplies at the terminal strip
has the benefit of providing *multiple solar panels and charge controllers* to the logger,
so that no single failure can take down the whole system. It also has the benefit of reallocating disproportionally
distributed power.

For example, here is a summary of the power supply after CENT tower is combined with CENT aspirated fan.

========  =================  =================  ====================  =================  ===============
Station   System Daily (Ah)  Battery Bank (Ah)  < 30% DOD for 7 days  System Daily (Wh)  Solar Panel (W)
========  =================  =================  ====================  =================  ===============
CENT Twr  12.06              624                281                   144.7              200
CENT Asp  14.63              416                488                   175.56             100
CEN Comb  26.69              1040               890                   320.26             300
========  =================  =================  ====================  =================  ===============

.. todo::
    This requires time to rewire the system.

:Cost: 0

Low Voltage Disconnect
^^^^^^^^^^^^^^^^^^^^^^^
Low voltage disconnects provide protection for batteries to prevent overdischarge. This is important for long term
budgeting and allows the stations to recover automatically, without the need for an operator to replace the batteries
before the station comes back on line.

Currently, many installations do not have LVD's. Xantrex charge controllers can be set as either a charge controller,
or a load controller. Installations with these could be repurposed to use a Sunsaver as a charge controller and a
Xantrex as a load controller.

Below is a possible reconfiguration. Components that will need to be purchased are highlighted:

==========   ==================   ====================================
Station      Current Controller   Suggested Reconfiguration
==========   ==================   ====================================
UPLO Shltr   Xantrex C35          Chrg: Sunsaver 10; Load: Xantrex c35
UPLO SA      Sunsaver 10          Sunsaver 20L
CENT Shltr   Sunsaver 10          Chrg: Sunsaver 10; Load: Xantrex c35
CENT Twr     Xantrex C35          **MPPT**
CENT ASP     Sunsaver 10          **MPPT**
VAN Twr      Xantrex C35          Chrg: Sunsaver 10; Load: Xantrex c35
VARA SA      Sunsaver 10          **MPPT**
VARA Lgr     Sunsaver 10          **MPPT**
==========   ==================   ====================================

.. note::
    MPPT charge/load controllers have ~20-25% improvement in
    solar power production over currently installed PWM controllers.

.. todo::
    Reconfigure current charge/load controllers. Purchase and install new charge/load controllers.

:Cost:
    Price: $240 Sunsaver MPPT-15L

    Quantity: 4 New MPPT charge/load controllers

    Total: **$960**


Replace/Remove Unnecessary Radios
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Both UPLO and CENT have 2 radios, one each at the tower and the shelter. UPLO also had a 3rd radio on the tower as a
relay. In both cases, empty conduit runs between the two. By connecting the two loggers via an ethernet switch, one
radio can be removed at CENT and 2 have already been removed at UPLO. This would leave only 1 network access point per
station, but in the event of a network failure, the data would remain securely backed up on both the logger and the
card.

VARA, CENT, and VAN have high draw Tranzeo radios. These can be replaced with lower draw Nano-locos. This would
significantly
reduce
the power demands at these stations.

=========   ===============   ================   =========   ============   ============   ===========
Station     System Daily Ah   Network Daily Ah   Netwrok %   Change in Ah   New Daily Ah   % Reduction
=========   ===============   ================   =========   ============   ============   ===========
UPLO Twr    10.35             9.0                79          -3.98          6.37           38
UPLO Shlt   5.76              5.0                87          -3.98          1.77           69
CENT Shlt   2.0               1.3                64          -0.73          1.75           13
CENT Twr    9.43              8.0                86          -3.05          6.42           32
VARA        8.65              8                  93          -3.05          5.64           35
VAN Twr     9.8               8.0                82          -3.05          6.75           45
=========   ===============   ================   =========   ============   ============   ===========

.. todo::
    VAN,VARA, CENT - Replace Tranzeo radios with Nano-Loco radios

        CENT, UPLO (completed) - Remove shelter radios and run ethernet cable from shelter to tower

:Cost: 2 Nano-locos must be purchased.

Control Network and Fan Operation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Network infrastructure and aspirated fans draw more current than any other component because they are running
constantly. It is important to be able to shut off their power to save critical components during low power operations.
Radios and fans serve different purposes that require different control logic, but both can be controlled with the same
mechanism. By powering both components from the 12 v switch on the logger (SW12V), they can be switched off to save
logger function. A manual override is also needed, to cut all power regardless of program logic.

.. Note::
    The NL116 should be turned off with the loggers. The card and ethernet port combined draw 1.03 Ah per day.

Example Code
""""""""""""""
.. code-block:: vbnet

    'Example from RV50 mannual
    'SW12 Voltage Control
    'Turn ON SW12 between 0900 hours and 1700 hours
    'for 20 minutes every 60 minutes
    If BattV >12.5
    If TimeIsBetween(540,1020,1440,Min) And TimeIsBetween(0,15,60,Min) Then
    SW12State=True
    Else
    SW12State=False
    EndIf
    'Always turn OFF SW12 if battery drops below 11.5 volts
    If BattV<11.5 Then SW12State=False
    'Set SW12-1 to the state of 'SW12State' variable
    SW12(1,SW12State,0)

Example Wiring Configuration
"""""""""""""""""""""""""""""
.. figure:: _figures/ComponentShutOff_Design.png
    :align: left
    :height: 650px

.. todo::
    * Develop control logic
        * Minimal power mode where the radio and fan are shut down
        * Different radio schedules for different power levels
    * Rewire radios and fan to 12v switch
    * Purchase and install manual switches

:Cost:
    Quality switches cost $15 - $50.

    Quantity 14 (4 switches x [VAN, UPLOTwr, CENTTwr] + 2 at VARA).

    Minimum **$210**

Radio Controls
"""""""""""""""
Unlike the aspirated fan, the network infrastructure does not need to be on all of the time. The network can be cycled
on and off to transmit for discrete periods of time. During periods of low power the radio could turn on for one hour
every afternoon. During good weather the radio could turn on for 5 minutes each hour. **Using the radio changes
discussed** above, here are the **additional effects** of reducing radio transmission time. Redux % represents the
reduction in Ah as :math:`\frac{NewRadioDaily Ah - ReducedTransmissionTime Ah}{2015 Ah}`

=========   ==================   ====================  ===================   ===================   =========================
Station     New Radio Daily Ah   Daily Ah 24 x 5 min   Daily Ah 1 x 40 min   Daily Ah 4 x 20 min   Redux % (2015 Ah)
=========   ==================   ====================  ===================   ===================   =========================
UPLO Twr    6.37                 1.77                  1.49                  1.63                  44 - 47
UPLO Shlt   1.77                 NL116 Power must be   timed with Twr
CENT Twr    6.42                 1.82                  1.54                  1.68                  49 - 52
CENT Shlt   1.75                 NL116 Power must be   timed with Twr
VARA        5.64                 1.29                  0.84                  1.07                  49 - 53
VAN Twr     6.75                 2.15                  1.87                  2.01                  41 - 43
=========   ==================   ====================  ===================   ===================   =========================

.. todo::
    Change logger programs to shut down NL116 and radios and provide power at limited intervals

:Cost: 0

Separate Housing For Charge Controllers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Currently charge controllers are mounted horizontally on top of the batteries in the battery boxes. This can potentially
damage or destroy the charge controllers due to overheating, and the corrosive gases released as the batteries charge.
As described by the Morningstar Corporation:

.. Warning::

    **Equipment Damage or Risk of Explosion**

    Never install the SunSaver in an enclosure with vented/flooded batteries. Battery fumes are flammable and will
    corrode and destroy the Sunsaver circuits.

    **Equipment Damage**

    When installing the Sunsaver in an enclosure, ensure sufficient ventilation. Installation in a sealed enclosure will
    lead to over-heating and a decreased product lifetime.

    Mounting the controller on a horizontal surface does not provide optimal airflow and could lead to overheating.

    **Safety Precautions**

    Install the Sunsaver in a location that prevents casual contact. The SunSaver heatsink can become very hot during
    operation

Because of the importance of convective cooling it is important that they be placed in vented enclosures. However, due
to the high amount of mist and fog at the stations, an outer shelter is also needed, possibly sealed like reference
stands.

.. todo::
    * Construct outer shelters
    * Mount shelters to tower
    * Rewire solar panels and batteries

:Cost: 4 vented enclosures will cost a minimum of $200

Projected Power Use
----------------------
The summary below is the projected power supply and demand after the upgrades discussed above are implemented.
Aspirated fans are now included in estimates for all towers. Numbers
are estimates using the methods described previously and under powered components are highlighted.

========  =================  =================  ====================  =================  ===============   ===============
Station   System Daily (Ah)  Battery Bank (Ah)  <30% DOD for 7 days   System Daily (Wh)  Solar Panel (W)   Winter Need (W)
========  =================  =================  ====================  =================  ===============   ===============
CENT Twr  16.45              1040               548.33                197.40             300               286.09
CENT Shl  1.75               416                58.33                 21                 **50.2**          **72.75**
UPLO Twr  16.40              1248               546.67                196.80             360               285.22
UPLO Shl  1.77               416                59.00                 21.24              240               30.78
VAN Twr	  16.78              1456               559.33                201.36             **262.6**         **291.83**
VARA      1.29               208                43.00                 15.48              131               22.43
========  =================  =================  ====================  =================  ===============   ===============

This projection indicates that VAN Tower and CENT shelter will be underpowered during the winter. In both cases,
if the smallest solar panel were replaced with 1- 140W panel, the station should generate close to the daily power
demand during winter.

The estimates used here are rough estimates, that do not include such details as: voltage drop, heat loss, temperature
corrected battery Ah, temperature corrected solar output, and light intensity corrected solar output. The estimates
provided are likely inaccurate, but they are precisely calculated. Their primary function is to allow improvements to be
prioritized.

