****************
Power Summary
****************

In 2015 a multi-year upgrade was completed at primary meteorological sations at the HJ Andrews Experimental 
Forest. Prior to the upgrade, stations used a small number of old, Edlog based loggers to link a large array 
of sensors using an extensive network of cables. This centralized data transfer within each station.

The upgrade introduced a dispersed digital network where multiple loggers independently connect to the network
at each site. However, adding independent
loggers at each site, each with independent radios, created an increased demand on infrastructure. In the 
winter of 2015/2016 the low sunlight conditions in combination with increased demand lead to station crashes. 

My assessment of the stations 
as a whole is that there are not enough checks and controls to ensure the integrity of the system of 
physical components paired with the digital network. Power is limiting, but increasing power alone
will continue to leave the station vulnerable to crashes. Further, a large push to increase power is 
impractical, because the physical infrastructure is dispersed  within each site, requiring separate 
upgrades for: 1)towers, 2)shelters, 3)stand alone rain gauges, 4)aspirated fans

Key System Problems
====================
The key issues all stem from added components that are unregulated; their power usage is not monitored,
and they draw continuously, whereas most sensors draw power for a fraction of each logger scan.

Unknown System Current
-----------------------
There is currently no tracking of system power (Watts) or current (Ampre). This makes it difficult to know if
the system is functioning properly. It is especially hard to identify when current usage exceeds charging. It 
also makes it difficult to assess battery health or changes in panel efficiency.

Limited or No Battery Protection
-----------------------------------
There are few Low Voltage Disconnects (LVD's) on batteries, and no digital or manual shut offs for any part of 
the met station. This allows stations to fully discharge batteries. Once fully discharged, they cannot be recharged
once solar power returns. Therefore, the station will not regain power until the batteries are replaced, even during
sunny periods. This has the potential benefit of logging data until the last possible moment, but ruins the batteries,
which has a high monetary cost.


Fan Status
-----------
The largest single draw is the 43502_ aspirated radiation shield. At all stations these have an independent 
power supply, so the draw does not impact other components. However, the current to operate the fan is 120% 
to 190% of the power budget for the loggers. Estimates suggest that 4 - 6 months per year, there is not enough solar
power to fully support daily fan current, and that batteries will only last 6 - 9 days. It is likely from Oct through
March that the fan turns
off
when the sun isn't out. This could cause battery damage that would prevent charging even in summer. However there is no
way to verify when the thermistor is being aspirated. This is a
data quality issue that should be monitored. 

Due to the high draw it is important to have the ability to shut the fan off when power supplies are low.

Network Connectivity
----------------------
Network connectivity is the second largest draw on the power system. The radios and NL116's consume from 75% 
to 95% of current at each station daily, plus an additional 10% loss in conversion from 12v to 24v. However, 
the radios are only transmitting for a fraction of the time for which they are running. This is wasted power.

Without the ability to turn the radios off, there are no options for operators when power is low. It is
important to provide multiple options to allow operators to address a variety of expected or unforeseen
problems.

Mid-Priority Problems
======================
* Poor solar sites (low duration and intensity)
* Vegetative overgrowth and shading
* Reduced solar panel efficiency
	* ~1% per year
	* solar panel ages are unknown
* Inadequate charge controller placement
	* use of charge controllers outside of designed temperature range
	* use of unsealed charge controllers in moist environments and exposed to moisture vented from batteries.
	* exposure to overheating (low ventilation, proximity to batteries)
	* exposure to corrosive battery acids and gases
* Use of PWM charge controllers
	* Inefficient in cold weather
	* Inefficient in indirect light
* Degraded wiring
	* damaged and disintegrated insulation
	* exposure to sunlight and moisture
	
Proposed Improvements
======================
1. Installation of shunts to track and monitor system power usage.
2. Program logger to control power to fans and radios.
	a. Set a low power shut off.
	b. Schedule radio transmitions for discrete periods of time.
3. Provide manual power shut offs to all components.
4. Improve charge controllers:
	a. Prioritize removal of charge controllers without LVD.
	b. All replacement charge controllers will be MPPT for cold temperature and low light efficiency.
	c. Use available enclosures to house and protect charge controllers separately from batteries.
5. Where possible, all updated portions should use electrical conduit to protect wiring.
6. Remove or reduce vegetation that blocks solar panels.
7. In some specific cases, solar panels should be moved to new locations, requiring some reconfiguration.






.. _43502 : https://www.campbellsci.com/pn43502