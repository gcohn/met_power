*****************
Station Status
*****************

.. toctree::
   :maxdepth: 2
   
   system_demand
   environmental_conditions

Power Guidelines
^^^^^^^^^^^^^^^^^
The worst week experienced across sites can be expected to have 0.69 kWh of sunlight per day, assuming no blocking
vegetation and correct panel orientation. Sunlight may stay only slightly above this level for 3 or more weeks.
This means that the minimum solar sizing should be **daily power(Wh) draw / 0.69kWh.**

Additionally, batteries should be able to maintain during weeks where amounts of sun are below the 10th percentile due
to snow cover, and short days. To survive 1 week with no solar power, without allowing the batteries to exceed 30%
depth of discharge means the battery must sustain more than 70% of it's 20hr Ah rating, or **(daily Ah x 7 days) / 0.3**
. Campbell Scientific uses a more conservative factor, replacing 0.3 with 0.21.

Currently, solar panel and battery placement do not match power requirements. Resources need to be reallocated so that
power supplies are placed where there is a corresponding electrical demand.